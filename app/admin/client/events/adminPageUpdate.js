Template.adminPageUpdate.onRendered(function() {
  var page;
  page = Post.findOne({
    _id: FlowRouter.getParam('id')
  });
  $('form').form('set values', page);
});

Template.adminPageUpdate.events({
  "submit form": function(event, template) {
    var fields;
    event.preventDefault();
    fields = {
      title: event.target.title.value,
      slug: event.target.slug.value,
      body: event.target.body.value
    };
    Post.update(event.target._id.value, {
      $set: fields
    });
    return FlowRouter.go('admPages');
  }
});
