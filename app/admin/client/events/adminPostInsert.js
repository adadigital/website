Template.adminPostInsert.events({
  "submit form": function(event, template) {
    var fields;
    event.preventDefault();
    fields = $('form').form('get values');
    fields.type = 'post';
    Post.insert(fields);
    return FlowRouter.go('admPosts');
  }
});
