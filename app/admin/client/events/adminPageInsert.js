Template.adminPageInsert.onRendered(function(){
  
});
Template.adminPageInsert.events({
  "submit form": function(event, template) {
    var fields;
    event.preventDefault();
    fields = $('form').form('get values');
    fields.type = 'page';
    Post.insert(fields);
    return FlowRouter.go('admPages');
  }
});
