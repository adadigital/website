Template.adminPosts.events({
	"click .remove": function(event, template) {
		event.preventDefault();
		var me = this;
		$('.ui.modal').modal({
			transition: 'vertical flip',
			onApprove: function() {
				Post.remove({_id: me._id});
			}
		}).modal('show');
	}
});
