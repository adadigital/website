Template.root.helpers({
  'posts': function() {
    return Post.find({
      type: 'post'
    }, {
      limit: 10,
      skip: (Session.get('page') - 1) * 10
    });
  }
});
