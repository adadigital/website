Template.single.helpers({
  'post': function() {
    return Post.findOne({
      slug: FlowRouter.getParam('slug')
    });
  },
  'comments': function() {
    var post = Post.findOne({
      slug: FlowRouter.getParam('slug')
    });
    if (post.type === 'post') {
      return Comment.find({
        postId: post._id
      });
    } else {
      return false;
    }
  }
});
