Template.page.helpers({
  'post': function() {
    return Post.findOne({
      slug: FlowRouter.getParam('slug')
    });
  }
});
