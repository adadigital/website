FlowRouter.route('/', {
  name: 'root',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    this.register('allComments', Meteor.subscribe('allComments'));
  },
  action: function(params, queryParams) {
    Session.set('page', queryParams.page | 1);
    return BlazeLayout.render('homeLayout', {
      banner: 'banner',
      main: 'root',
      left: 'leftMenu'
    });
  }
});
FlowRouter.route('/:slug', {
  name: 'page',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    this.register('postComments', Meteor.subscribe('postComments'),params.slug);
  },
  action: function(params, queryParams) {
    Session.set('page', queryParams.page | 1);
    return BlazeLayout.render('postLayout', {
      main: 'page',
      left: 'leftMenu'
    });
  }
});

FlowRouter.route('/blog', {
  name: 'root',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    this.register('allComments', Meteor.subscribe('allComments'));
  },
  action: function(params, queryParams) {
    Session.set('page', queryParams.page | 1);
    return BlazeLayout.render('blogLayout', {
      banner: 'banner',
      main: 'blog',
      left: 'leftMenu'
    });
  }
});
FlowRouter.route('/blog/:slug', {
  name: 'single',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    this.register('postComments', Meteor.subscribe('postComments'),params.slug);
  },
  action: function(params, queryParams) {
    Session.set('page', queryParams.page | 1);
    return BlazeLayout.render('blogLayout', {
      banner: 'banner',
      main: 'single',
      left: 'leftMenu'
    });
  }
});
