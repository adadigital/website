//var adminRoutes;
adminRoutes = FlowRouter.group({
  prefix: '/admin',
  name: 'admin'
});

adminRoutes.route('/', {
  name: 'admRoot',
  action: function(params, queryParams) {
    return BlazeLayout.render('adminLayout', {
      'main': 'adminRoot',
      'left': 'adminMenu'
    });
  }
});

adminRoutes.route('/posts', {
  name: 'admPosts',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    //this.register('allComments', Meteor.subscribe('allComments'));
  },
  action: function(params, queryParams) {
    Session.set('page', queryParams.page | 1);
    return BlazeLayout.render('adminLayout', {
      'main': 'adminPosts',
      'left': 'adminMenu'
    });
  }
});

adminRoutes.route('/posts/insert', {
  name: 'admPostInsert',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    //this.register('allComments', Meteor.subscribe('allComments'));
  },
  action: function(params, queryParams) {
    return BlazeLayout.render('adminLayout', {
      'main': 'adminPostInsert',
      'left': 'adminMenu'
    });
  }
});

adminRoutes.route('/posts/:id', {
  name: 'admPostUpdate',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    //this.register('allComments', Meteor.subscribe('allComments'));
  },action: function(params, queryParams) {
    return BlazeLayout.render('adminLayout', {
      'main': 'adminPostUpdate',
      'left': 'adminMenu'
    });
  }
});

adminRoutes.route('/pages', {
  name: 'admPages',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    //this.register('allComments', Meteor.subscribe('allComments'));
  },action: function(params, queryParams) {
    Session.set('page', queryParams.page | 1);
    return BlazeLayout.render('adminLayout', {
      'main': 'adminPages',
      'left': 'adminMenu'
    });
  }
});

adminRoutes.route('/pages/insert', {
  name: 'admPageInsert',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    //this.register('allComments', Meteor.subscribe('allComments'));
  },action: function(params, queryParams) {
    return BlazeLayout.render('adminLayout', {
      'main': 'adminPageInsert',
      'left': 'adminMenu'
    });
  }
});

adminRoutes.route('/pages/:id', {
  name: 'admPageUpdate',
  subscriptions: function(params, queryParams) {
    this.register('allPosts', Meteor.subscribe('allPosts'));
    //this.register('allComments', Meteor.subscribe('allComments'));
  },action: function(params, queryParams) {
    return BlazeLayout.render('adminLayout', {
      'main': 'adminPageUpdate',
      'left': 'adminMenu'
    });
  }
});
